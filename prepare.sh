#!/bin/bash
set -e

PATH="/usr/local/go/bin":$PATH
VERSION=$(echo $(cat RELEASE || git describe --always) | sed -e "s/^v//" | sed -r "s/-[0-9]{1,3}-g[0-9a-f]{5,10}//") # try RELEASE file, then git description

IFS='-' read -ra ARR<<<"${ARCH_TRIPLET}"
ARCH=$(echo ${ARR[0]}|sed -e "s/armhf/arm/" -e "s/aarch/arm/" -e "s/x86_/amd/")
SOURCE="syncthing-linux-${ARCH}-v${VERSION}"

CACHE=${ROOT}/cache

echo "Building Syncthing Web ..."
echo "VERSION: ${VERSION}"
echo "ARCH   : ${ARCH}"
echo "SOURCE : ${SOURCE}.tar.gz"
echo "CACHE  : ${CACHE}"
echo "-----------------------"

mkdir -p ${INSTALL_DIR}/bin
mkdir -p ${CACHE}

# echo "ls ${CACHE}/${SOURCE}/syncthing || wget -nv -O- https://github.com/syncthing/syncthing/releases/download/v${VERSION}/${SOURCE}.tar.gz | tar -xzvC ${CACHE}"
ls ${CACHE}/${SOURCE}/syncthing || wget -nv -O- https://github.com/syncthing/syncthing/releases/download/v${VERSION}/${SOURCE}.tar.gz | tar -xzvC ${CACHE}

cp ${CACHE}/${SOURCE}/syncthing ${INSTALL_DIR}/bin/syncthing-web
# #cp ${ROOT}/syncthing/assets/logo-only.svg ${INSTALL_DIR}/syncthing.svg

cp ${ROOT}/clickable/[Ss]yncthing?* ${INSTALL_DIR}/
cp -r ${ROOT}/qml ${INSTALL_DIR}/
