# syncthing-clickable

[![License: MPLv2](https://img.shields.io/badge/License-MPLv2-blue.svg)](https://opensource.org/licenses/MPL-2.0)

A wrapper of [Syncthing](https://github.com/syncthing/syncthing) for Ubuntu Touch

<img src="graphics/screenshot-1.png" alt="screenshot 1" width="200" />
<img src="graphics/screenshot-3.png" alt="screenshot 3" width="200" />
<img src="graphics/screenshot-5.png" alt="screenshot 5" width="200" />

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/syncthing.syncthing)

# Building 

### Dependencies
- Docker
- Android tools (for adb)
- Python3 / pip3
- Clickable (get it from [here](https://clickable-ut.dev/en/latest/index.html))

Use Clickable to build and package Syncthing as a Click package ready to be installed on Ubuntu Touch

### Build instructions
Make sure you clone the project with
`git clone https://gitlab.com/arubislander/syncthing-clickable.git --recursive`. Alternatively, run
`git submodule init && git submodule update` in the project folder.

To test the build on your workstation:
```
$   clickable desktop
```

To run on a device over SSH:
```
$   clickable --ssh [device IP address]
```

To build for a different architecture:
```
$   clickable -a [arch] build
```
where `arch` is one of: `amd64`, `arm64` or `armhf`.

For more information on the several options see the Clickable [documentation](https://clickable-ut.dev/en/latest/index.html)

# TODO
* Write a sentinel file in the configration directory to determine if jobs need refreshing or not.
  At the moment the jobs are rewritten everytime the application starts. This is wasteful.
* Allow external URL's to be opened in Morph browser

# License

The project is licensed under the [MPLv2](LICENSE).