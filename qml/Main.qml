/*
 * Copyright (C) 2020  Terence Sambo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtWebEngine 1.6

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'syncthing.syncthing'
    automaticOrientation: true
    backgroundColor: "transparent"
    anchorToKeyboard: !myDialog.visible

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
        id: mainStack

        anchors {
           fill: parent
        }
    }

    Component.onCompleted: {
        mainStack.push(page)
    }

    Page {

        id: page
        anchors.fill: parent
        visible: false

        WebEngineView {
            //property alias context: _webview.profile
            //This factor should be around 2.75 for mobile
            //property real factor: units.gu(1) / 8.4

            id: _webview
            anchors.fill: parent
            //z: -6
            //profile: webcontext
            zoomFactor: units.gu(1) / 8.4
            url: "http://localhost:8384"

            onAuthenticationDialogRequested: function(request) {
                request.accepted = true;
                myDialog.request = request // keep the reference to the request
                myDialog.accept.connect(request.dialogAccept);
                myDialog.reject.connect(request.dialogReject);
                myDialog.visible = true;
            }
        }

        Dialog {
            id: myDialog
            title: i18n.tr("Sign in")
            property var request: null
            visible: false

            signal accept(string username, string password)
            signal reject()

            UbuntuShape {
                source: Image {
                    source: "https://syncthing.net/img/logo-horizontal.svg"
                }
            }

            TextField {
                id: username
                placeholderText: i18n.tr("username")
            }

            TextField {
                id: password
                placeholderText: i18n.tr("password")
                echoMode: TextInput.Password
            }
            
            Button {
                text: i18n.tr("Sign In")
                color: theme.palette.normal.positive
                enabled: username.text != "" && password.text != ""

                onClicked: {
                    myDialog.accept(username.text, password.text)
                    PopupUtils.close(myDialog)
                } 
            }
            
            Button {
                text: i18n.tr("Cancel")
                onClicked:{
                    myDialog.reject()
                    PopupUtils.close(myDialog)
                    Qt.quit()
                }
            }
        }
    }
}
