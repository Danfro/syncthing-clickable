#!/bin/bash

GOPATH=$(pwd)/go-build

IFS='-' read -ra ARR<<<"${ARCH_TRIPLET}"
ARCH=$(echo ${ARR[0]}|sed -e "s/armhf/arm/" -e "s/aarch/arm/" -e "s/x86_/amd/")
#SUFFIX=$(echo ${ARCH} | grep amd64 || echo "other")
VERSION=$(echo $(cat ${ROOT}/RELEASE || git describe --always) | sed -e "s/^v//" | sed -r "s/-[0-9]{1,3}-g[0-9a-f]{5,10}//") # try RELEASE file, then git description

sed -e "s/VERSION/${VERSION}/" ${ROOT}/clickable/manifest.json.in > ${INSTALL_DIR}/manifest.json

echo "Building syncthing ..."
echo "VERSION: ${VERSION}"
echo "ARCH   : ${ARCH}"
echo "-----------------------"
echo "/usr/local/go/bin/go build -o ${INSTALL_DIR}/bin/syncthing ${ROOT}/src/syncthing/main.go "

pushd ${ROOT}/src/syncthing

# case "${ARCH}" in
#     arm|arm64) 
        echo "export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:/usr/include/${ARCH_TRIPLET}/qt5/QtCore/5.12.9/"
        export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:/usr/include/${ARCH_TRIPLET}/qt5/QtCore/5.12.9/
#         ;;
# esac

/usr/local/go/bin/go build -ldflags="-s -w" -o ${INSTALL_DIR}/bin/syncthing main.go
if [ "$?" -ne "0" ]; then
    exit -1
fi
popd

echo "Building syncthing daemon ..."
echo "VERSION: ${VERSION}"
echo "ARCH   : ${ARCH}"
echo "-----------------------"
echo "/usr/local/go/bin/go build -o ${INSTALL_DIR}/bin/syncthingd ${ROOT}/src/syncthingd/main.go"

pushd ${ROOT}/src/syncthingd
/usr/local/go/bin/go build -ldflags="-s -w" -o ${INSTALL_DIR}/bin/syncthingd main.go
popd
