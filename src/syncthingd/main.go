package main

import (
	"fmt"
	"log"
	"os/exec"
	"time"
)

const wakeDuration = time.Minute * 10
const waitDuration = wakeDuration
const sleepDuration = time.Hour - wakeDuration

const processSyncthing = "syncthing"
const processSyncthingWeb = "syncthing-web"
const eventStart = "syncthing-webd_start"
const eventStop = "syncthing-webd_stop"
const eventTick = "syncthingd_tick"

func main() {
	switch {
	case isRunning(processSyncthing) && !isRunning(processSyncthingWeb):
		emit(eventStart)
		log.Println("main.go: Started web app for gui.")
		fallthrough

	case isRunning(processSyncthing) && isRunning(processSyncthingWeb):
		log.Printf("main.go: Checking again in %d minutes...\n", int(waitDuration.Minutes()))
		time.Sleep(waitDuration)

	case !isRunning(processSyncthing) && !isRunning(processSyncthingWeb):
		emit(eventStart)
		log.Println("main.go: Started web app in background.")
		log.Printf("main.go: Checking again in %d minutes...\n", int(wakeDuration.Minutes()))
		time.Sleep(wakeDuration)

	case !isRunning(processSyncthing) && isRunning(processSyncthingWeb):
		emit(eventStop)
		log.Println("main.go: Stopped web app.")
		log.Printf("main.go: Checking again in %d minutes...\n", int(sleepDuration.Minutes()))
		time.Sleep(sleepDuration)
	}
	emit(eventTick)
}

func emit(event string) {
	checkError(wrapError(fmt.Sprintf("Could not emit event: %s", event), exec.Command("initctl", "emit", event).Run()))
}

func isRunning(processName string) bool {
	err := exec.Command("pidof", processName).Run()
	return err == nil
}

func wrapError(message string, err error) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf("%s: %s", message, err)
}

func checkError(err error) {
	if err != nil {
		log.Fatalf("syncthingd: Fatal Error: %s", err)
	}
}
