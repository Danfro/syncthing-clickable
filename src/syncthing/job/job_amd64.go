// +build amd64 386

package job

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
)

func startService(jobSyncthingd string) (*exec.Cmd, error) {
	// check if job is already running. Do nothing if so
	output, err := exec.Command("ps", "-C", jobSyncthingd).Output()
	if err == nil && strings.Contains(string(output), jobSyncthingd) {
		log.Println("job_amd64.go: Nothing to do; daemon already running.")
		return nil, fmt.Errorf("job already running")
	}

	if err = os.Setenv("STNOUPGRADE", "1"); err != nil {
		return nil, fmt.Errorf("could not set environment variable STNOUPGRADE")
	}

	cmd := exec.Command(path.Join("bin", "syncthing-web"), "-no-browser")
	err = cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("could not start daemon: %s", err)
	}

	return cmd, nil
}

func CreateJob(name, source string) (*Job, error) {
	job := &Job{
		cmd:  nil,
		name: name,
		path: path.Clean(source),
	}

	return job, nil
}

func (j *Job) Start() error {
	log.Println("job_amd64.go: Starting job ...")
	if j.cmd != nil {
		j.Stop()
	}
	cmd, err := startService(path.Base(j.path))
	if err != nil {
		return fmt.Errorf("Could not start the service: %s", err)
	}
	j.cmd = cmd
	log.Println("job_amd64.go: Job started")
	return nil
}

func (j *Job) Stop() error {
	log.Println("job_amd64.go: Stopping job ...")
	err := j.cmd.Process.Signal(os.Interrupt)
	if err != nil {
		return fmt.Errorf("Could not stop the service: %s", err)
	}
	log.Println("job_amd64.go: Job stopped")
	return nil
}
