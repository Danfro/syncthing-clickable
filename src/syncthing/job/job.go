package job

import (
	"fmt"
	"log"
	"net"
	"os/exec"
	"time"

	"github.com/nanu-c/qml-go"
)

type Job struct {
	Root qml.Object
	cmd  *exec.Cmd
	name string
	path string
}

func (j *Job) WaitTilInitialized() error {
	return waitForDaemon()
}

func waitForDaemon() error {
	log.Println("main.go: Waiting for initialization to complete ...")
	easOffFactor := time.Duration(2)
	waitDuration := time.Second
	doneChan := make(chan interface{})
	maxWait := time.Second * 30
	go func() {
		for {
			// check if job is already running. Do nothing if so
			_, err := net.Dial("tcp", "127.0.0.1:8384")
			if err != nil {
				time.Sleep(waitDuration)
				waitDuration *= easOffFactor
				log.Println("main.go: Still waiting ...", err)
				continue
			}

			log.Println("main.go: Initialization complete!")
			break
		}
		close(doneChan)
	}()

	select {
	case <-doneChan:
		break
	case <-time.After(maxWait):
		return fmt.Errorf("Timed out while waiting for server")
	}
	return nil
}
