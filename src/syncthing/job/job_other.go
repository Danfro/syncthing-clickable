// +build !amd64
// +build !386

package job

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
)

const job_extension = ".conf"

func CreateJob(name, source string) (*Job, error) {
	// check if source exists
	// create destination path
	// copy source to dest if necessary

	home, err := os.UserHomeDir()
	if err != nil {
		return nil, fmt.Errorf("Could not retrieve user home dir: %s", err)
	}

	dest := path.Join(home, ".config", "upstart", name+job_extension)

	err = os.MkdirAll(path.Dir(dest), 0755)
	if err != nil {
		return nil, fmt.Errorf("Could not create job dir: %s", err)
	}

	job := &Job{
		cmd:  nil,
		name: name,
		path: path.Clean(dest),
	}

	if path.Ext(source) != job_extension {
		source = source + job_extension
	}
	sourceInfo, err := os.Stat(source)
	if err != nil {
		return nil, fmt.Errorf("Could not stat %s: %s", source, err)
	}

	destInfo, err := os.Stat(dest)
	if err == nil && !destInfo.ModTime().Before(sourceInfo.ModTime()) {
		log.Printf("job_other.go: Nothing to do; %s is current\n", dest)
		return job, nil
	}

	if err = job.Stop(); err != nil {
		return nil, err
	}

	err = os.MkdirAll(path.Dir(dest), 0755)
	if err != nil {
		return nil, fmt.Errorf("Could not create job dir: %s", err)
	}

	if err = exec.Command("cp", "-f", source, dest).Run(); err != nil {
		return nil, fmt.Errorf("Could not copy to %s: %s", dest, err)
	}

	if err = os.Chmod(dest, 0644); err != nil {
		return nil, fmt.Errorf("Could not change permisions of %s: %s", path.Base(dest), err)
	}

	return job, nil
}

func (j *Job) Start() error {
	if err := emit("syncthingd_tick"); err != nil {
		return fmt.Errorf("Could not start job: %s", err)
	}
	return nil

}

func (j *Job) Stop() error {
	if err := emit("syncthingd_kill"); err != nil {
		return fmt.Errorf("Could not stop job: %s", err)
	}
	return nil
}

func emit(event string) error {
	if err := exec.Command("initctl", "emit", event).Run(); err != nil {
		return fmt.Errorf("Could emit event %s: %s", event, err)
	}
	return nil
}
