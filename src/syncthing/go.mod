module nl.arubislander/syncthing

go 1.14

require (
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/nanu-c/qml-go v0.0.0-20201002212753-238e81315528
	golang.org/x/sys v0.0.0-20210402192133-700132347e07 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
