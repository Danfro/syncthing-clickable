/*
 * Copyright (C) 2020  Terence Sambo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"log"
	"os"
	"path"

	"github.com/nanu-c/qml-go"
	"nl.arubislander/syncthing/job"
)

const jobSyncthingWebd = "syncthing-webd"
const jobSyncthingd = "syncthingd"

func main() {

	err := qml.Run(run)
	checkError(err)
}

func run() error {
	// create Job
	pwd, err := os.Getwd()
	checkError(err)

	_, err = job.CreateJob(jobSyncthingWebd, path.Join(pwd, jobSyncthingWebd))
	checkError(err)

	job, err := job.CreateJob(jobSyncthingd, path.Join(pwd, jobSyncthingd))
	checkError(err)

	// start job early so other initialization can take place in parallel
	checkError(job.Start())

	// create enginge and hook job.Stop() to quit slot
	engine := qml.NewEngine()

	// Load Main.qml
	component, err := engine.LoadFile("qml/Main.qml")
	checkError(err)

	context := engine.Context()
	context.SetVar("job", job)

	// wait for job to be initialized before showing the main window
	err = job.WaitTilInitialized()
	checkError(err)

	win := component.CreateWindow(nil)
	job.Root = win.Root()

	engine.On("quit", func() {
		checkError(job.Stop())
		win.Hide()
	})

	win.Show()
	win.Wait()

	return nil
}

func checkError(err error) {
	if err != nil {
		log.Fatalf("main.go: Fatal Error: %s", err)
	}
}
